import React, { Component } from 'react';
import { CssBaseline, Paper, Typography } from '@material-ui/core';
import TopAppBar from '../TopAppBar/TopAppBar';
import logo from './logo.svg';
import './global.css';


class App extends Component {
  render() {
    return (
      <>
        <CssBaseline />
        <TopAppBar />
        <div className="App">
          <Paper className="App-paper">
            <img src={logo} className="App-logo" alt="logo" />
            <Typography variant="h6" style={{ margin: '64px 0px 0px 0px' }}>
              src/containers/App/App.js에서 이 문구를 수정해 보세요.
            </Typography>
          </Paper>
        </div>
      </>
    );
  }
}

export default App;
